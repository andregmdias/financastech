package br.com.giannatech.financatech.model.repository;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.giannatech.financatech.model.entity.Usuario;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@AutoConfigureTestDatabase( replace = Replace.NONE )
public class UsuarioRepositoryTest {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	TestEntityManager entityManager;
	
	
	@Test
	public void verificaEmailExistente() {
	
		Usuario usuario = Usuario.builder().nome("usuario").email("usuario@gmail.com").senha("1234").build();
		entityManager.persist(usuario);
		
		boolean resultado = usuarioRepository.existsByEmail("usuario@gmail.com");
		
		Assertions.assertThat(resultado).isTrue();
		
	}
	
	
	@Test
	public void verificaEmailNaoExists() {
		boolean resultado = usuarioRepository.existsByEmail("andregmdias@gmail.com");
		
		Assertions.assertThat(resultado).isFalse();
		
	}
	
	
	@Test
	public void persisteUsuarioNoBanco() {
		Usuario usuario = Usuario.builder().nome("usuario").email("usuario@emailcom").senha("1234").build();
		Usuario usuarioSalvo = usuarioRepository.save(usuario);
		
		Assertions.assertThat(usuarioSalvo).isNotNull();
	}
	
	@Test
	public void buscaUsuarioPorEmail () {
		Usuario usuario = Usuario.builder().nome("usuario").email("usuario@emailcom").senha("1234").build();
		entityManager.persist(usuario);
		
		Optional <Usuario> usuarioBuscado = usuarioRepository.findByEmail("usuario@emailcom");
		
		Assertions.assertThat(usuarioBuscado.isPresent()).isTrue();
		
	}
	
	@Test
	public void naoEncontraUsuarioPorEmail () {
		Optional <Usuario> usuarioBuscado = usuarioRepository.findByEmail("usuario@emailcom");
		
		Assertions.assertThat(usuarioBuscado.isPresent()).isFalse();
		
	}
	
	
}
