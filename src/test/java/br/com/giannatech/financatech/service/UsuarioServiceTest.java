package br.com.giannatech.financatech.service;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test.None;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.giannatech.financatech.exception.EmailJaCadastradoException;
import br.com.giannatech.financatech.exception.ErroAutenticacaoException;
import br.com.giannatech.financatech.model.entity.Usuario;
import br.com.giannatech.financatech.model.repository.UsuarioRepository;
import br.com.giannatech.financatech.service.impl.UsuarioServiceImpl;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class UsuarioServiceTest {

	@SpyBean
	UsuarioServiceImpl usuarioService;

	@MockBean
	UsuarioRepository usuarioRepository;

	@Test
	public void cadastroComEmailValido() {
		
		Mockito.when(usuarioRepository.existsByEmail(Mockito.anyString())).thenReturn(false);
		
		usuarioService.validarEmail("email@email.com");
		
		Assertions.assertThatExceptionOfType(None.class);
	}
	@Test
	public void lancaExcessaoEmailCadastrado() {

		Mockito.when(usuarioRepository.existsByEmail(Mockito.anyString())).thenReturn(true);

		Assertions.assertThatExceptionOfType(EmailJaCadastradoException.class);
	}


	@Test
	public void cadastrarUsuarioComSucesso() {
		Mockito.doNothing().when(usuarioService).validarEmail(Mockito.anyString());
		Usuario usuario = Usuario.builder().id(1l).nome("andre luiz").email("andreluiz@email.com").senha("1234").build();
		Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
		Usuario usuarioSalvo = usuarioService.cadastrarUsuario(usuario);
		Assertions.assertThat(usuarioSalvo).isNotNull();
		Assertions.assertThat(usuarioSalvo.getId()).isEqualTo(1l);
		Assertions.assertThat(usuarioSalvo.getNome()).isEqualTo("andre luiz");
		Assertions.assertThat(usuarioSalvo.getEmail()).isEqualTo("andreluiz@email.com");
		Assertions.assertThat(usuarioSalvo.getSenha()).isEqualTo("1234");
	}

//	@Test
//	public void naoCadastrarUsuarioComSucesso() {
//		Usuario usuario = Usuario.builder().email("andreluiz@email.com").build();
//		Mockito.doThrow(EmailJaCadastradoException.class).when(usuarioService).validarEmail(usuario.getEmail());
//		Mockito.verify(usuarioService, Mockito.never()).cadastrarUsuario(usuario);
//		usuarioService.cadastrarUsuario(usuario);
//		
//	}

	@Test()
	public void autenticaUsuarioComSucesso() {
		Usuario usuario = Usuario.builder().nome("andre luiz").email("andreluiz@email.com").senha("1234").build();
		Mockito.when(usuarioRepository.findByEmail(usuario.getEmail())).thenReturn(Optional.of(usuario));
		Usuario result = usuarioService.autenticar("andreluiz@email.com", "1234");
		Assertions.assertThat(result).isNotNull();
	}
	
	@Test
	public void usuarioNaoEncontrado() {

		Mockito.when(usuarioRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.empty());
		
		Throwable throwable = Assertions.catchThrowable(() -> usuarioService.autenticar("andreluiz@email.com", "1234")); 
		Assertions.assertThat(throwable).isInstanceOf(ErroAutenticacaoException.class).hasMessage("Usuario não encontrado");
	}

	@Test
	public void erroSenhaAutenticacaoUsuario() {
		Usuario usuario = Usuario.builder().nome("andre luiz").email("andreluiz@email.com").senha("1234").build();
		Mockito.when(usuarioRepository.findByEmail("andreluiz@email.com")).thenReturn(Optional.of(usuario));
		Throwable throwable = Assertions.catchThrowable(() -> usuarioService.autenticar("andreluiz@email.com", "12345")); 
		Assertions.assertThat(throwable).isInstanceOf(ErroAutenticacaoException.class).hasMessage("Senha inválida para o e-mail informado");
	}
}
