package br.com.giannatech.financatech.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.giannatech.financatech.api.dto.UsuarioDTO;
import br.com.giannatech.financatech.exception.EmailJaCadastradoException;
import br.com.giannatech.financatech.exception.ErroAutenticacaoException;
import br.com.giannatech.financatech.model.entity.Usuario;
import br.com.giannatech.financatech.service.UsuarioService;

@RestController
@RequestMapping("/api/usuarios")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class UsuarioResource {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/autenticar")
	public ResponseEntity autenticar( @RequestBody UsuarioDTO usuarioDTO ) {
		try {
			Usuario usuarioAutenticado = usuarioService.autenticar(usuarioDTO.getEmail(), usuarioDTO.getSenha());
			return ResponseEntity.ok(usuarioAutenticado);
		}catch (ErroAutenticacaoException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@PostMapping("/cadastrar")
	public ResponseEntity cadastrarUsuarios( @RequestBody UsuarioDTO usuarioDTO ) {
	
		Usuario usuario = Usuario.builder()
				.nome(usuarioDTO.getNome())
				.email(usuarioDTO.getEmail())
				.senha(usuarioDTO.getSenha()).build();
		try {
			Usuario usuarioSalvo = usuarioService.cadastrarUsuario(usuario);
			return new ResponseEntity(usuarioSalvo,HttpStatus.CREATED);
		}catch (EmailJaCadastradoException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
		
	}
	
}
