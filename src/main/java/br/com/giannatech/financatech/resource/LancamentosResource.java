package br.com.giannatech.financatech.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.giannatech.financatech.api.dto.LancamentoDTO;
import br.com.giannatech.financatech.exception.CampoInvalidoException;
import br.com.giannatech.financatech.exception.LancamentoNaoEncontradoException;
import br.com.giannatech.financatech.exception.UsuarioNaoEncontradoException;
import br.com.giannatech.financatech.model.entity.Lancamento;
import br.com.giannatech.financatech.model.entity.Usuario;
import br.com.giannatech.financatech.model.enums.StatusLancamento;
import br.com.giannatech.financatech.model.enums.TipoLancamento;
import br.com.giannatech.financatech.service.LancamentoService;
import br.com.giannatech.financatech.service.UsuarioService;

@RestController
@RequestMapping("/api/lancamentos")
@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
public class LancamentosResource {

	@Autowired
	private LancamentoService service;
	
	@Autowired 
	UsuarioService usuarioService;
	
	
	@GetMapping
	public ResponseEntity buscar(@RequestParam(value ="descricao", required = false) String descricao, @RequestParam(value = "mes", required = false) 
	Integer mes, @RequestParam(value = "ano", required = false) Integer ano, @RequestParam("usuario") Long idUsuario ) {
		Lancamento lancamentoFiltro = new Lancamento();
		lancamentoFiltro.setDescricao(descricao);
		lancamentoFiltro.setMes(mes);
		lancamentoFiltro.setAno(ano);
		Optional< Usuario > usuario = usuarioService.findById(idUsuario);
		if(! usuario.isPresent()) {
			return ResponseEntity.badRequest().body("Não foi encontrado usuário para o id informado");
		}else {
			lancamentoFiltro.setUsuario(usuario.get());
		}
		List<Lancamento> lancamentos = service.buscar(lancamentoFiltro);
		return ResponseEntity.ok(lancamentos);
	}
	
	
	@PostMapping
	@RequestMapping("/salvar")
	public ResponseEntity salvar( @RequestBody LancamentoDTO dto ) {
		Lancamento lancamentoSalvo = lancamentoConverter(dto);
		try {
			lancamentoSalvo = service.salvar(lancamentoSalvo);
			return new ResponseEntity(lancamentoSalvo, HttpStatus.CREATED);
		}catch (CampoInvalidoException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("{id}")
	public ResponseEntity atualizar( @PathVariable("id") Long id , @RequestBody LancamentoDTO dto ) {
		return service.findById(id).map( entity -> {
			try {
				Lancamento lancamento = lancamentoConverter(dto);
				lancamento.setId(dto.getId());
				service.atualizar(lancamento);
				return ResponseEntity.ok(lancamento);
			}catch (LancamentoNaoEncontradoException e) {
				return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
		}).orElseGet(() -> new ResponseEntity ( "Lançamento não encontrado na base de dados", HttpStatus.BAD_REQUEST ) );
		
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity apagarLancamento( @PathVariable("id")Long id, LancamentoDTO lancamento ) {
		return service.findById(id).map( entity -> {
			try {
				service.apagarLancamento(id);
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}catch (LancamentoNaoEncontradoException e) {
				return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
		}).orElseGet(() -> new ResponseEntity ( "Lançamento não encontrado na base de dados", HttpStatus.BAD_REQUEST ) );
		
	}
	
	private Lancamento lancamentoConverter( LancamentoDTO dto ) {
		Lancamento lancamento = new Lancamento();
		Usuario usuario = usuarioService.findById( dto.getId() ).orElseThrow( () -> new UsuarioNaoEncontradoException("Usuário não encontrado para o Id informado"));
		lancamento.builder()
				  .id(dto.getId())
				  .descricao(dto.getDescricao())
				  .ano(dto.getAno())
				  .mes(dto.getMes())
				  .usuario(usuario)
				  .tipo(TipoLancamento.valueOf(dto.getTipo()))
				  .status(StatusLancamento.valueOf(dto.getStatus()));
		return lancamento;
				  
	}
	
}
