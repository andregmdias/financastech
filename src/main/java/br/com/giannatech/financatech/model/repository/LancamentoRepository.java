package br.com.giannatech.financatech.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.giannatech.financatech.model.entity.Lancamento;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>{

}
