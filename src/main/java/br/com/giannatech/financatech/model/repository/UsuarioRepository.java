package br.com.giannatech.financatech.model.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.giannatech.financatech.model.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	
	Optional < Usuario > findByEmailAndSenha(String email, String senha);
	Optional < Usuario > findByEmail( String email );
	
	boolean existsByEmail( String email );

}
