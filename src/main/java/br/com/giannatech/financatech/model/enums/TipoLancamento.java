package br.com.giannatech.financatech.model.enums;

public enum TipoLancamento {
	RECEITA, DESPESA
}
