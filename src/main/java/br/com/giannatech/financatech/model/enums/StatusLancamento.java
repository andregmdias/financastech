package br.com.giannatech.financatech.model.enums;

public enum StatusLancamento {

	PENDENTE, CANCELADO, EFETIVADO
}
