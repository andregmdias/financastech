package br.com.giannatech.financatech.exception;

public class CampoInvalidoException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public CampoInvalidoException( String msg ) {
		super(msg);
	}

}
