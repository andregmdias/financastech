package br.com.giannatech.financatech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinancatechApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinancatechApplication.class, args);
	}

}
