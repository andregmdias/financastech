package br.com.giannatech.financatech.service;

import java.util.List;
import java.util.Optional;

import br.com.giannatech.financatech.model.entity.Lancamento;
import br.com.giannatech.financatech.model.enums.StatusLancamento;

public interface LancamentoService {

	Lancamento salvar( Lancamento lancameto );
	Lancamento atualizar ( Lancamento lancameto );
	Optional < Lancamento > findById ( Long id );
	void apagar ( Lancamento lancamento );
	List< Lancamento > buscar ( Lancamento lancamentoFiltro );
	void atualizarStatus ( Lancamento lancamento, StatusLancamento statusLancamento );
	void apagarLancamento(Long id);
	void validar( Lancamento lancamento );
}
