package br.com.giannatech.financatech.service;

import java.util.Optional;

import br.com.giannatech.financatech.model.entity.Usuario;

public interface UsuarioService {

	Usuario autenticar( String email, String senha );
	
	Usuario cadastrarUsuario( Usuario usuario );
	
	void validarEmail( String email );
	
	Optional< Usuario > findById ( Long id);
	
	
}
