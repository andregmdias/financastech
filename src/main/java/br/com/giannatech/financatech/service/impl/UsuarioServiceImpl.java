package br.com.giannatech.financatech.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.giannatech.financatech.exception.EmailJaCadastradoException;
import br.com.giannatech.financatech.exception.ErroAutenticacaoException;
import br.com.giannatech.financatech.exception.UsuarioNaoEncontradoException;
import br.com.giannatech.financatech.model.entity.Usuario;
import br.com.giannatech.financatech.model.repository.UsuarioRepository;
import br.com.giannatech.financatech.service.UsuarioService;

@Service 
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public Usuario autenticar(String email, String senha) {
		Optional < Usuario > usuarioOptional = usuarioRepository.findByEmail(email);
		if ( !usuarioOptional.isPresent() ) {
			throw new ErroAutenticacaoException("Usuario não encontrado");
		}else if(! usuarioOptional.get().getSenha().equals(senha) ){
			throw new ErroAutenticacaoException("Senha inválida para o e-mail informado");
		}else {
			return usuarioOptional.get();
		}
	}

	@Override
	@Transactional
	public Usuario cadastrarUsuario(Usuario usuario) {
		validarEmail(usuario.getEmail());
		usuarioRepository.save(usuario);
		return usuario;
	}

	@Override
	public void validarEmail(String email) {
		boolean existe = usuarioRepository.existsByEmail(email);
		if( existe ) {
			throw new EmailJaCadastradoException("Usuário já cdastrado com este e-mail");
		}
	}

	@Override
	public Optional<Usuario> findById(Long id) {
		Optional< Usuario > result = usuarioRepository.findById(id); 

		return result;
	}
}
