package br.com.giannatech.financatech.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.giannatech.financatech.exception.CampoInvalidoException;
import br.com.giannatech.financatech.model.entity.Lancamento;
import br.com.giannatech.financatech.model.enums.StatusLancamento;
import br.com.giannatech.financatech.model.repository.LancamentoRepository;
import br.com.giannatech.financatech.service.LancamentoService;

@Service
public class LancamentoServiceImpl implements LancamentoService{

	@Autowired
	private LancamentoRepository lancamentoRepository;
	
	@Override
	@Transactional
	public Lancamento salvar(Lancamento lancamento) {
		validar(lancamento);
		return lancamentoRepository.save(lancamento);
	}

	@Override
	@Transactional
	public Lancamento atualizar(Lancamento lancamento) {
		Objects.requireNonNull(lancamento.getId());
		validar(lancamento);
		lancamento.setStatus(StatusLancamento.PENDENTE);
		return lancamentoRepository.save(lancamento);
	}

	@Override
	public void apagar(Lancamento lancamento) {
		Objects.requireNonNull(lancamento.getId());
		lancamentoRepository.delete(lancamento);
	}

	@Override
	@Transactional( readOnly = true )
	public List<Lancamento> buscar(Lancamento lancamentoFiltro) {
		Example<Lancamento> example = Example.of( lancamentoFiltro, ExampleMatcher.matching().withIgnoreCase().withStringMatcher( StringMatcher.CONTAINING ) );
		return lancamentoRepository.findAll(example);
	}

	@Override
	public void atualizarStatus(Lancamento lancamento, StatusLancamento statusLancamento) {
		lancamento.setStatus(statusLancamento);
		atualizar(lancamento);
	}

	@Override
	public void validar(Lancamento lancamento) {

		if( lancamento.getDescricao() == null || lancamento.getDescricao().trim().equals("") ) {
			throw new CampoInvalidoException("Informe uma Descrição válida");
		}
		
		if(lancamento.getMes() == null || lancamento.getMes() < 1 || lancamento.getMes() > 12 ) {
			throw new CampoInvalidoException("Informe um Mês válido.");
		}
		if(lancamento.getAno() == null || lancamento.getAno().toString().length() != 4 ) {
			throw new CampoInvalidoException("Informe um Ano válido.");
		}
		if( lancamento.getUsuario() == null || lancamento.getUsuario().getId() == null ) {
			throw new CampoInvalidoException("Informe um Usuário.");
		}
		if( lancamento.getValor() == null || lancamento.getValor().compareTo(BigDecimal.ZERO) < 1 ) {
			throw new CampoInvalidoException("Informe um Valor maior que zero.");
		}
		if( lancamento.getTipo() == null ) {
			throw new CampoInvalidoException("Informe um Tipo de Lançamento");
		}
		
		
	}

	@Override
	public Optional  <Lancamento > findById( Long id ) {
		return lancamentoRepository.findById(id);
	}

	@Override
	public void apagarLancamento(Long id) {
		lancamentoRepository.deleteById(id);
		
	}

}
